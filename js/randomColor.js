function randomColors(){
    window.setInterval(function(){
    var randomColor = '#'+ ('000000' + Math.floor(Math.random()*16777215).toString(16)).slice(-6);
    $('body').css({
      'background-color' : randomColor,
    });

    }, 2000);
}

function solidColor(){
    $('body').css({
      'background-image' : 'linear-gradient(to right, rgba(145, 179, 228) , rgba(170, 157, 200))'
    });
}