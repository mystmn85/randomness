// Thanks to https://miniwebtool.com/random-quote-generator/ for the quotes.
function quotes(){
    var quotes = new Array(
    "Civilization is a race between education and catastrophe.",
    "Hope is not the conviction that something will turn out well but the certainty that something makes sense, regardless of how it turns out.",
    "If you have something to do that is worthwhile doing, don't talk about it, but do it. After you have done it, your friends and enemies will talk about it.",
    "America is a construction of mind, not of race or inherited class or ancestral territory.",
    "Life's meaning has always eluded me and I guess always will. But I love it just the same.",
    "Better be despised for too anxious apprehensions, than ruined by too confident security.",
    "How we spend our days is, of course, how we spend our lives.",
    "When I was younger I could remember anything, whether it had happened or not.",
    "I could not at any age be content to take my place in a corner by the fireside and simply look on.",
    "Britannia needs no bulwarks, No towers along the steep; Her march is o'er the mountain waves, Her home is on the deep.",
    "When I would re-create myself, I seek the darkest wood, the thickest and most interminable and to the citizen, most dismal, swamp. I enter as a sacred place, a Sanctum sanctorum. There is the strength, the marrow, of Nature.",
    "Don't shut yourself up in a band box because you are a woman, but understand what is going on, and educate yourself to take part in the world's work, for it all affects you and yours.",
    "The human foot is a masterpiece of engineering and a work of art.",
    "OUTCOME, n. A particular type of disappointment ... judged by the outcome, the result. This is immortal nonsense; the wisdom of an act is to be juded by the light that the doer had when he performed it.",
    "You may gain temporary appeasement by a policy of concession to violence, but you do not gain lasting peace that way.",
    "Democracy is still upon its trial. The civic genius of our people is its only bulwark.",
    "Successful men are influenced by desire for pleasing results. Failures are influenced by desire for pleasing methods.",
    "Old age takes away from us what we have inherited and gives us what we have earned.",
    "Anyone nit-picking enough to write a letter of correction to an editor doubtless deserves the error that provoked it.",
    "Many men absorbed in business show such a rare quality of culture that we are surprised at it. The reason invariably is partly because hard work and even the weariness it leaves carry a nobility with them, but also because there is no room in such lives for inferior mental occupation.",
    );
    var randNumber = Math.floor(Math.random()*(quotes.length));
    $('.quote').append(quotes[randNumber]);
}